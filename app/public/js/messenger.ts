
export class Messenger
{
	msg: string;

	constructor(msg: string)
	{
		this.msg = msg;
	}

	message()
	{
		return this.msg;
	}
}