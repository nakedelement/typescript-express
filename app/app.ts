
import { rootRoutes } from "./routes/root_routes";

declare var require: any
declare var __dirname: any
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');

class App
{
    port: number;
    app: any;

    constructor(port: number)
    {
        this.port = port;
        this.app = express();

        //register middleware to serve static pages
	    this.app.use(express.static(path.join(__dirname, 'public')));
	    this.app.use(express.static(path.join(__dirname, '../../../bower_components')));
	    this.app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.png')));

	    //Register middleware to serve views
	    this.app.set('views', path.join(__dirname, 'views'));
	    this.app.set('view engine', 'ejs');

	    this.app.use(rootRoutes());
    }

    start()
    {
    	this.app.listen(this.port);
    }
}


let app = new App(3000); 
app.start();