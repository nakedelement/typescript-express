declare var require: any
var express = require('express');
var router = express.Router();

export function rootRoutes() {
    router.get('/', function(req: any, res: any) {
	    res.render('index');
	});

    router.get('/message1', function(req: any, res: any) {
	    res.render('message1/index');
	});

	router.get('/message2', function(req: any, res: any) {
	    res.render('message2/index');
	});

	return router;
}