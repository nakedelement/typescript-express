var gulp = require("gulp");
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");
var clean = require('gulp-clean');
var es = require('event-stream');
var browserify = require("browserify");
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer')
var uglify = require('gulp-uglify');
var runSequence = require('run-sequence');

gulp.task('clean', function () {
    return gulp.src(['dist','build'], {read: false})
        .pipe(clean());
});

gulp.task("build", ['clean'], function () {
    return gulp.src('./app/**/*.ts')
        .pipe(tsProject())
        .js.pipe(gulp.dest('./build/app'));
});

gulp.task('browserify', ['build'], function()
{
    var files = [
        'message1/index.js',
        'message2/index.js'
    ];

    return es.merge.apply(null, files.map(function(entry) {

         return browserify({
         basedir: './build',
        entries: ['app/public/js/' + entry],
    })
    .bundle()     
    .pipe(source('app/public/js/' + entry))    
    .pipe(gulp.dest("./dist/dev/"))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest("./dist/prod/"));
        
     }));
});

gulp.task("copy-css", function () {
    return gulp.src('app/public/css/**/')
         .pipe(gulp.dest('./dist/dev/app/public/css/'))
         .pipe(gulp.dest('./dist/prod/app/public/css/'));
});

gulp.task("copy-images", function () {
    return gulp.src('app/public/images/**/')
         .pipe(gulp.dest('./dist/dev/app/public/images/'))
         .pipe(gulp.dest('./dist/prod/app/public/images/'));
});

gulp.task("copy-views", function () {
    return gulp.src('app/views/**/')
         .pipe(gulp.dest('./dist/dev/app/views/'))
         .pipe(gulp.dest('./dist/prod/app/views/'));
});

gulp.task("copy-app", function () {
    return gulp.src(['build/app/**/', '!build/app/public/**'])
         .pipe(gulp.dest('./dist/dev/app/'))
         .pipe(gulp.dest('./dist/prod/app/'));
});

gulp.task('default', function(callback)
{
     runSequence('browserify', 
                 ['copy-css', 'copy-images', 'copy-views', 'copy-app'],
                 callback);
});